#include "Player.h"
#include "Weapon.h"
#include <iostream>
#include <string>
#include <vector>

Player* Player::instance = nullptr;

//Player::Player() : health(100), currentWeapon(WeaponType::Sword) {}

Player::~Player()
{
}

Player* Player::getInstance() {
    if (!instance) {
        instance = new Player();
    }
    return instance;
} 

void Player::setName(std::string name) {
    this->name = name;
}

std::string Player::getName() {
    return name;
}

void Player::setHealth(int health) {
    this->health = health;
}

int Player::getHealth() const {
    return health;
}

void Player::setStrength(int strength) {
    this->strength = strength;
}

int Player::getStrength() const {
    return strength;
}

void Player::setMagic(int magic) {
    this->magic = magic;
}

int Player::getMagic() const {
    return magic;
}

void Player::setPlayerClass(PlayerClass playerClass) {
    this->playerClass = playerClass;

    switch (playerClass) {
    case PlayerClass::Warrior:
        setHealth(100);
        setStrength(10);
        setMagic(5);
        break;
    case PlayerClass::Mage:
        setHealth(80);
        setStrength(5);
        setMagic(15);
        break;
        // Add any additional player classes and their attributes here
    }
}

PlayerClass Player::getPlayerClass() const{
    return playerClass;
}

void Player::setCurrentWeapon(WeaponType weapon) {
    this->currentWeapon = static_cast<WeaponType>(weapon);

    // Update player attributes based on the new weapon
    switch (weapon) {
    case WeaponType::Sword:
        setStrength(getStrength() + 5);
        setMagic(getMagic() - 2);
        break;
    case WeaponType::Staff:
        setStrength(getStrength() - 2);
        setMagic(getMagic() + 5);
        break;
        // Add any additional weapons and their effects here
    }
}

void Player::printPlayerStats(const Player& player) {
    std::cout << "Player stats:" << std::endl;
    //std::cout << "Class: " << player.getPlayerClass() << std::endl;

    switch (player.getPlayerClass())
    {
    case PlayerClass::Warrior:
        std::cout << "Class: Warrior" << std::endl;
        break;

    case PlayerClass::Mage:
        std::cout << "Class: Mage" << std::endl;
        break;
    }

    //std::cout << "Health: " << player.getHealth() << std::endl;
    //std::cout << "Strength: " << player.getStrength() << std::endl;
    //std::cout << "Magic: " << player.getMagic() << std::endl;
    
}

WeaponType Player::getCurrentWeapon() const{
    return currentWeapon;
}



/* void Player::takeDamage(Weapon damage)
{
    int damageAmount = damage.getDamage();
    health -= damageAmount;
}*/


//const std::vector<Loot>& getInventory() const {
   // return inventory;
//}