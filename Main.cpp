#include <iostream>
#include "GameManager.h"


int main() {

    // Create a new game manager
    GameManager gameManager;

    // Start the game
    gameManager.start();

    // Play the game until it's over
    while (!gameManager.isGameOver()) {
        gameManager.battle();
    }

    // Game is over
    gameManager.gameOver();

    return 0;
}