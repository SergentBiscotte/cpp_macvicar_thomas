#pragma once

#include <string>
#include "Enemy.h"


//Represente weapon
enum class WeaponType {
    Sword,
    Staff,
    Axe
};

//Stock weapon infos
class Weapon {
public:
    Weapon(std::string name, int damage);
    Weapon(WeaponType weaponType, int damage);

    //std::string getName() const;
    //void setName(const std::string& name);
    //int getDamage() const;
    int damage; 
    //void setDamage(int damage);

    friend std::ostream& operator<<(std::ostream& os, const Weapon& weapon);
    

private:
    std::string name;
    
    WeaponType weaponType;
};
