#include "GameManager.h"
#include "Weapon.h"
#include <iostream>
#include <cstdlib>


GameManager::~GameManager()
{
    for (auto enemy : enemies)
    {
        delete enemy;
    }
}

GameManager& GameManager::getInstance()
{
    static GameManager instance;
    return instance;
}

GameManager::GameManager() : player(*(Player::getInstance())) {
    //player = std::make_unique<Player>();
    Over = false;
}

void GameManager::start()
{
    std::cout << "Welcome to the game!" << std::endl;

    std::cout << "Please enter your name: ";
    std::string playerName;
    std::cin >> playerName;
    player.setName(playerName);

    std::cout << "Please select a class (1 for Warrior, 2 for Mage): ";
    int playerClass;
    std::cin >> playerClass;

    if (playerClass == 1)
    {
        player.setPlayerClass(PlayerClass::Warrior);
        player.setHealth(100);
        player.setStrength(20);
        player.setMagic(5);
        player.setCurrentWeapon(WeaponType::Sword);
    }
    else if (playerClass == 2)
    {
        player.setPlayerClass(PlayerClass::Mage);
        player.setHealth(80);
        player.setStrength(5);
        player.setMagic(20);
        player.setCurrentWeapon(WeaponType::Staff);
    }
    else
    {
        std::cout << "Invalid input. Please try again." << std::endl;
        start();
        return;
    }

    std::cout << std::endl << "Welcome, " << player.getName() << "! Your adventure begins now." << std::endl;
    std::cout << player << std::endl;

    addEnemy(new Enemy("Goblin 1", 20, 5, 0, EnemyType::Goblin));
    addEnemy(new Enemy("Skeleton 1", 30, 8, 0, EnemyType::Skeleton));

    battle();
}

/*void GameManager::battle()
{
    std::cout << "A wild enemy appears!" << std::endl;

    Enemy* currentEnemy = enemies.back();
    currentEnemy->printEnemyStats(currentEnemy);

    while (currentEnemy->getHealth() > 0 && !isGameOver())
    {
        std::cout << "Select an action (1 for attack, 2 for use item): ";
        int action;
        std::cin >> action;

        if (action == 1)
        {
            Weapon playerWeapon = player.getCurrentWeapon();
            int damage = calculateDamage(player.getStrength(), playerWeapon.damage);
            currentEnemy->takeDamage(damage);

            std::cout << "You hit the enemy for " << damage << " damage." << std::endl;

            if (currentEnemy->getHealth() <= 0)
            {
                std::cout << "You defeated the enemy!" << std::endl;
                player.addItemToInventory(currentEnemy->dropLoot());
                removeEnemy(currentEnemy);
                return;
            }

            int enemyDamage = calculateDamage(currentEnemy->getStrength(), currentEnemy->getCurrentWeapon().damage);
            player.takeDamage(Weapon(enemyDamage, currentEnemy->getCurrentWeapon().type));

            std::cout << "The enemy hits you for " << enemyDamage << " damage." << std::endl;
        }
        else if (action == 2) //Action for inventory
        {
            std::cout << "Inventory Incoming" << std::endl;
        }
        else
        {
            std::cout << "Invalid input. Please try again." << std::endl;
        }
    }

    if (isGameOver())
    {
        gameOver();
    }
} */


void GameManager::gameOver()
{
    std::cout << "Game over." << std::endl;
    Over = true;
}

bool GameManager::isGameOver()
{
    return Over;
}





void GameManager::addEnemy(Enemy* enemy) {
    enemies.push_back(enemy);
}

void GameManager::removeEnemy(Enemy* enemy) {
    auto it = std::find(enemies.begin(), enemies.end(), enemy);
    if (it != enemies.end()) {
        enemies.erase(it);
        delete enemy;
    }
}

//Turn based battle takes place.
//Player chooses an enemy to attack and ennemies attack the player after his turn
void GameManager::battle() {
    std::cout << "A battle has started!" << std::endl;

    while (!enemies.empty() && player.getHealth() > 0) {
        std::cout << "Player's turn:" << std::endl;

        // Display the list of enemies
        std::cout << "Enemies:" << std::endl;
        for (int i = 0; i < enemies.size(); i++) {
            std::cout << i + 1 << ". " << enemies[i]->getName() << std::endl;
        }

        // Ask the player which enemy to attack
        int enemyIndex;
        do {
            std::cout << "Choose an enemy to attack (1-" << enemies.size() << "): ";
            std::cin >> enemyIndex;
        } while (enemyIndex < 1 || enemyIndex > enemies.size());

        // Player attacks the chosen enemy
        Enemy* enemy = enemies[enemyIndex - 1];
        std::cout << "Player attacks " << enemy->getName() << std::endl;

        //DEALING DAMAGE TO ENNEMIES NOT WORLING
        
        //int damage = player.getCurrentWeapon().getDamage();
        //enemy->takeDamage(damage);
        //std::cout << "Player deals " << damage << " damage to " << enemy->getName() << std::endl;

        // Check if enemy is dead and drops loot
        if (enemy->getHealth() <= 0) {
            std::cout << enemy->getName() << " has been defeated!" << std::endl;
            Loot loot = enemy->dropLoot();
            std::cout << enemy->getName() << " dropped a " << loot.name << std::endl;
            //player.getInventory().push_back(loot);
            removeEnemy(enemy);
        }

        // Enemies' turn: attacks player and deal damage
        std::cout << "Enemies' turn:" << std::endl;
        for (Enemy* enemy : enemies) {
            std::cout << enemy->getName() << " attacks Player" << std::endl;
            
            //DEALING DAMAGE NOT WORKING
            
            //int damage = enemy->getCurrentWeapon().getDamage();
            //player.takeDamage(damage);
            //std::cout << enemy->getName() << " deals " << damage << " damage to Player" << std::endl;
        }
    }

    // Game over
    gameOver();
}
