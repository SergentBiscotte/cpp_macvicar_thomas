#include "Weapon.h"
#include <iostream>

Weapon::Weapon(std::string name, int damage) {
    this->name = name;
    this->damage = damage;
}

Weapon::Weapon(WeaponType weaponType, int damage) {
    switch (weaponType) {
    case WeaponType::Sword:
        name = "Sword";
        break;
    case WeaponType::Staff:
        name = "Staff";
        break;
    case WeaponType::Axe:
        name = "Axe";
        break;
    }
    this->damage = damage;
}

/*std::string Weapon::getName() const {
    return name;
} */

//int Weapon::getDamage() const {
//    return damage;
//} 

/*std::ostream& operator<<(std::ostream& os, const Weapon& weapon) {
    os << weapon.getName() << " (Damage: " << weapon.getDamage() << ")";
    return os;
} */
 /*std::ostream& operator<<(std::ostream& os, WeaponType weaponType) {
    os << WeaponType(weaponType);
    return os;
} */