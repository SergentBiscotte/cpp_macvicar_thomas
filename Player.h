#pragma once

#include <string>
#include <iostream>
#include <vector>
#include "Weapon.h"


enum class PlayerClass {
    Warrior,
    Mage
};

class Player {
private:
    
    static Player* instance;
    std::string name;
    int health;
    int strength;
    int magic;
    PlayerClass playerClass ;
    WeaponType currentWeapon;
 
    
    //mutable std::vector<Loot> inventory;

    friend class GameManager;

public:

    ~Player();
    static Player* getInstance();
    void setName(std::string name);
    std::string getName();
    void setHealth(int health);
    int getHealth() const;
    void setStrength(int strength);
    int getStrength() const;
    void setMagic(int magic);
    int getMagic() const;
    void setPlayerClass(PlayerClass playerClass);
    PlayerClass getPlayerClass() const;
    void setCurrentWeapon(WeaponType weapon);
    WeaponType getCurrentWeapon() const;
    //void takeDamage(Weapon damage);
    void printPlayerStats(const Player& player);


    //const std::vector<Loot>& getInventory() const;


    friend std::ostream& operator<<(std::ostream& os, const Player& player) 
    {
        os << "Player stats:" << std::endl;
        //os << "Class: " << player.playerClass << std::endl;
        os << "Strength: " << player.strength << std::endl;
        os << "Magic: " << player.magic << std::endl;
        return os;
    

    }


};
