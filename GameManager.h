#pragma once
#include <vector>
#include "Player.h"
#include "Enemy.h"
#include "Weapon.h"

class GameManager {
public:
    static GameManager& getInstance();
    void start();
    void battle();
    void gameOver();
    void addEnemy(Enemy* enemy);
    void removeEnemy(Enemy* enemy);
    bool isGameOver();
    int calculateDamage(Player player, Enemy enemy);
    int calculateDamage2(Enemy enemy, Player player);
    GameManager();
    ~GameManager();
private:
    
    
    GameManager(const GameManager&) = delete;
    GameManager& operator=(const GameManager&) = delete;
    
    std::vector<Enemy*> enemies;
    Player player;
    bool Over;
};