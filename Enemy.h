#pragma once

#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include "Weapon.h"

enum class EnemyType {
    Goblin,
    Skeleton
};

enum class LootType {
    Weapon,
    Potion,
    MagicScroll
};

struct Loot {
    LootType type;
    std::string name;
};

class Enemy {
private:
    std::string name;
    int health;
    int strength;
    int magic;
    EnemyType enemyType;
    std::vector<Loot> lootTable;
    //WeaponType currentWeapon;


public:
    Enemy(std::string name, int health, int strength, int magic, EnemyType enemyType);
    std::string getName();
    int getHealth();
    int getStrength();
    int getMagic();
    void takeDamage(int damage);
    EnemyType getEnemyType();
    Loot dropLoot();
    void printEnemyStats(Enemy* enemy);

    //WeaponType getCurrentWeapon() const;
    //void setCurrentWeapon(WeaponType weapon);
};