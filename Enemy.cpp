#include "Enemy.h"
#include <iostream>

Enemy::Enemy(std::string name, int health, int strength, int magic, EnemyType enemyType) {
    this->name = name;
    this->health = health;
    this->strength = strength;
    this->magic = magic;
    this->enemyType = enemyType;
    //this->currentWeapon = currentWeapon;

    // Initialize the enemy's loot table based on its type
    switch (enemyType) {
    case EnemyType::Goblin:
        lootTable.push_back({ LootType::MagicScroll, "Fire Ball" });
        lootTable.push_back({ LootType::Potion, "Health Potion" });
        break;
    case EnemyType::Skeleton:
        lootTable.push_back({ LootType::MagicScroll, "Ice Shard" });
        lootTable.push_back({ LootType::Potion, "Mana Potion" });
        lootTable.push_back({ LootType::Weapon, "Axe" });
        break;
        // Add any additional enemy types and their loot tables here
    }
}

std::string Enemy::getName() {
    return this->name;
}

int Enemy::getHealth() {
    return this->health;
}

int Enemy::getStrength() {
    return this->strength;
}

int Enemy::getMagic() {
    return this->magic;
}

EnemyType Enemy::getEnemyType() {
    return this->enemyType;
}

void Enemy::takeDamage(int damage)
{
    this->health -= damage;

    if (this->health <= 0)
    {
        this->health = 0;
    }
}

Loot Enemy::dropLoot() {
    // Generate a random index to select a loot item from the enemy's loot table
    int index = rand() % lootTable.size();

    // Remove the selected loot item from the loot table to prevent duplicates
    Loot droppedLoot = lootTable[index];
    lootTable.erase(lootTable.begin() + index);

    return droppedLoot;
}

void Enemy::printEnemyStats(Enemy* enemy)
{
    std::cout << "Name: " << enemy->getName() << std::endl;
    std::cout << "Type: ";

    switch (enemy->getEnemyType())
    {
    case EnemyType::Goblin:
        std::cout << "Goblin";
        break;

    case EnemyType::Skeleton:
        std::cout << "Skeleton";
        break;
    }

    std::cout << std::endl;
    std::cout << "Health: " << enemy->getHealth() << std::endl;
    std::cout << "Strength: " << enemy->getStrength() << std::endl;
}

//WeaponType Enemy::getCurrentWeapon() const {
//    return currentWeapon;
//}

//void Enemy::setCurrentWeapon(WeaponType weapon) {
//    currentWeapon = weapon;
//}